package com.wdpwp.match;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wdpwp.stattrack.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zarna on 4/4/15.
 */
public class MatchAdapter extends ArrayAdapter {

    public JSONArray list;

    public MatchAdapter(Context context, JSONArray list){
        super(context, R.layout.match_view, new String[list.length()]);
        this.list = list;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(getContext());

        View            matchView = inflater.inflate(R.layout.match_view, parent, false);
        JSONObject      item = null;
        try {
            item = list.getJSONObject(position);

            TextView    matchDate   = (TextView) matchView.findViewById(R.id.textViewMatchDate);
            TextView    matchGames  = (TextView) matchView.findViewById(R.id.textViewMatchGames);

            TextView    matchLive   = (TextView) matchView.findViewById(R.id.textViewMatchLive);

            TextView    matchT1name = (TextView) matchView.findViewById(R.id.textViewMatchT1name);
            TextView    matchT2name = (TextView) matchView.findViewById(R.id.textViewMatchT2name);
            ImageView   matchImgT1  = (ImageView) matchView.findViewById(R.id.imageViewT1);
            ImageView   matchImgT2  = (ImageView) matchView.findViewById(R.id.imageViewT2);

            matchDate.setText(item.getString("date"));
            matchGames.setText(item.getString("games"));
            matchT1name.setText(item.getJSONObject("t1").getString("name"));
            matchT2name.setText(item.getJSONObject("t2").getString("name"));

            if(item.getBoolean("live") == true){
                matchDate.setText("live");
            } else {
                matchDate.setText("upcomming");
            }

            matchImgT1.setImageResource(R.drawable.test);
            matchImgT2.setImageResource(R.drawable.test);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return matchView;

    }
}

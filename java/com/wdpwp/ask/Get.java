package com.wdpwp.ask;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by zarna on 4/4/15.
 */
public class Get {

    public static String collectData(String url){
        InputStream inputStream = null;
        String result = "";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse httpResponse = httpClient.execute(new HttpGet(url));
            inputStream = httpResponse.getEntity().getContent();

            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    public String getData(String url){

        BufferedReader  reader;
        String          data="";
        String          line;
        HttpClient      client;
        HttpGet         req;
        HttpResponse    res;
        StringBuffer    str;
        HttpEntity      entity;
        InputStream     stream;

        try{
            client = new DefaultHttpClient();
            req = new HttpGet(url);
            res = client.execute(req);

            entity = res.getEntity();
            //stream = res.getEntity().getContent();
            /*reader = new BufferedReader(new InputStreamReader(stream));
            str = new StringBuffer();
            while( (line = reader.readLine()) != null ){
                str.append(line);
            }
            reader.close();
            data = str.toString();
            */
            data = EntityUtils.toString(entity);
            return data;
        } catch(Exception e){
            e.printStackTrace();
        }

        return data;
    }
}

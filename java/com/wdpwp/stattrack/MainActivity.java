package com.wdpwp.stattrack;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.EditText;
import android.app.Activity;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.wdpwp.match.MatchAdapter;
import com.wdpwp.ask.Get;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity {

    EditText textContainer;
    String [] list = {"1", "2", "3", "4", "5"};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

        textContainer = (EditText) findViewById(R.id.textContainer);
        new AsyncGet().execute("http://wdpwp.com/api/unfinished");

	}

    private class AsyncGet extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            Get http = new Get();
            return http.getData(params[0]);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            try {
                ListAdapter     adapter;
                ListView        view = (ListView) findViewById(R.id.listViewMatches);
                String[]        strArray;

                JSONObject json = new JSONObject(result);
                JSONArray arr = json.getJSONArray("matches");

                String str = "";
                strArray = new String[arr.length()];

                for (int i=0; i < arr.length(); i++) {
                    JSONObject obj = arr.getJSONObject(i);
                    str += obj.getString("games");
                    strArray[i] = obj.getJSONObject("t1").getString("name")+" vs " +obj.getJSONObject("t2").getString("name");
                }

                adapter = new MatchAdapter(MainActivity.this, arr);

                view.setAdapter(adapter);
                textContainer.setText(str.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}

